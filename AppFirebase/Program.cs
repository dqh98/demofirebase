﻿using AppFirebase.Configurations;
using Firebase.Auth.Providers;
using Firebase.Auth;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Google.Cloud.Firestore;
using Google.Cloud.Storage.V1;
using AppFirebase.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var appSettingsSection = builder.Configuration.GetSection("AppSettings");
builder.Services.Configure<AppSettings>(appSettingsSection);
GlobalSettings.IncludeAppSettings(appSettingsSection.Get<AppSettings>());
GlobalSettings.CONTENT_ROOT_PATH = builder.Environment.ContentRootPath;

builder.Services.AddSwaggerGen(c =>
{
    var filePath = Path.Combine(AppContext.BaseDirectory, "API_App_Firebase.xml");
    
    c.IncludeXmlComments(filePath);
});

//Đăng ký FirebaseApp
var pathToServiceAccountKey = Path.Combine(GlobalSettings.CONTENT_ROOT_PATH, GlobalSettings.PATH_FILE_PRIVATE_KEY);
Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", pathToServiceAccountKey);
builder.Services.AddSingleton(FirebaseApp.Create());


builder.Services.AddSingleton(new FirebaseAuthClient(new FirebaseAuthConfig
{
    ApiKey = GlobalSettings.API_KEY,
    AuthDomain = GlobalSettings.AUTH_DOMAIN,
    Providers = new FirebaseAuthProvider[]
    {
        new EmailProvider(),
        new GoogleProvider()
    }
}));

// Firestore setup
//builder.Services.AddSingleton<IFirestoreService>(s => new FirestoreService(
//    FirestoreDb.Create(firebaseProjectName)
//    ));

// Storage setup
builder.Services.AddSingleton(StorageClient.Create());

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = Path.Combine(GlobalSettings.SECURE_TOKEN_GOOGLE_URL, GlobalSettings.NAME_PROJECT_FIREBASE);
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidIssuer = Path.Combine(GlobalSettings.SECURE_TOKEN_GOOGLE_URL, GlobalSettings.NAME_PROJECT_FIREBASE),
            ValidateAudience = true,
            ValidAudience = GlobalSettings.NAME_PROJECT_FIREBASE,
            ValidateLifetime = true
        };
    });

//builder.Services.AddSession();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
