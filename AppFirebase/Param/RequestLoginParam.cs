﻿namespace AppFirebase.Param
{
    public class RequestLoginParam
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
