﻿namespace AppFirebase.Configurations
{
    public class AppSettings
    {
        public string AuthSecret { get; set; }
        public string BasePath { get; set; }
        public string ApiKey { get; set; }
        public string AuthDomain { get; set; }
        public string PathFilePrivateKey { get; set; }
        public string SecureTokenGoogleUrl { get; set; }
        public string NameProjectFirebase { get; set; }
        public string MediaPath { get; set; }
    }
}
