﻿namespace AppFirebase.Configurations
{
    public class GlobalSettings
    {
        public static string CONTENT_ROOT_PATH { get; set; }
        public static string BASE_PATH { get; set; }
        public static string AUTH_SECRET { get; set; }
        public static string API_KEY { get; set; }
        public static string AUTH_DOMAIN { get; set; }
        public static string PATH_FILE_PRIVATE_KEY { get; set; }
        public static string SECURE_TOKEN_GOOGLE_URL { get; set; }
        public static string NAME_PROJECT_FIREBASE { get; set; }
        public static string MEDIA_PATH { get; set; }

        public static AppSettings AppSettings { get; set; } = new AppSettings();
        public static void IncludeAppSettings(AppSettings appSettings)
        {
            AppSettings = appSettings;
            BASE_PATH = appSettings.BasePath;
            AUTH_SECRET = appSettings.AuthSecret;
            API_KEY = appSettings.ApiKey;
            AUTH_DOMAIN = appSettings.AuthDomain;
            PATH_FILE_PRIVATE_KEY = appSettings.PathFilePrivateKey;
            SECURE_TOKEN_GOOGLE_URL = appSettings.SecureTokenGoogleUrl;
            NAME_PROJECT_FIREBASE = appSettings.NameProjectFirebase;
            MEDIA_PATH = appSettings.MediaPath;
        }
    }
}
