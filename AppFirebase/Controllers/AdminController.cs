﻿using Microsoft.AspNetCore.Mvc;
using FirebaseAdmin.Auth;
using AppFirebase.Param;
using Firebase.Auth;
using Swashbuckle.AspNetCore.Annotations;

namespace AppFirebase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [SwaggerTag("Quản lý tài khoản")]
    public class AdminController : ControllerBase
    {
        private readonly FirebaseAuthClient _firebaseAuth;
        public AdminController(FirebaseAuthClient firebaseAuth)
        {
            _firebaseAuth = firebaseAuth;
        }

        /// <summary>
        /// Đăng nhập
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login(RequestLoginParam request)
        {
            try
            {
                var result = await _firebaseAuth.SignInWithEmailAndPasswordAsync(request.Email, request.Password);

                var token = await result.User.GetIdTokenAsync();
                var info = result.User.Info;

                return new OkObjectResult(info);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Đăng ký
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("sign-up")]
        public async Task<IActionResult> SignUp(RequestRegisterParam request)
        {
            var auth = FirebaseAuth.DefaultInstance;
            
            try
            {
                var result = await auth.CreateUserAsync(new UserRecordArgs
                {
                    Email = request.Email,
                    Password = request.Password,
                });

                // Lấy thông tin người dùng đã đăng nhập
                var user = await auth.GetUserByEmailAsync(request.Email);
                return new OkObjectResult(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        /// <summary>
        /// Đăng xuất
        /// </summary>
        [HttpPost("sign-out")]
        public void SignOut()
        {
            _firebaseAuth.SignOut();
        }
    }
}
