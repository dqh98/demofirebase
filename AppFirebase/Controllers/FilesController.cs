﻿using Microsoft.AspNetCore.Mvc;
using AppFirebase.Models;
using AppFirebase.Configurations;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json;
using Google.Cloud.Storage.V1;
using System.Security.AccessControl;
using Firebase.Auth;
using FirebaseAdmin.Auth;
using Google.Api.Gax;

namespace AppFirebase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        //private readonly IFirebaseClient client = Utils.Config.ConfigClient();
        private readonly StorageClient _storageClient;
        private const string BucketName = "demofirebase-aa27e.appspot.com";

        public FilesController(StorageClient storageClient)
        {
            _storageClient = storageClient;
        }

        /// <summary>
        ///  Tải file lên firebase
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("upload-file")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            var name = Guid.NewGuid().ToString();
            
            try
            {
                using var stream = new MemoryStream();
                await file.CopyToAsync(stream);

                var blob = await _storageClient.UploadObjectAsync(BucketName, name, file.ContentType, stream);

                if (blob is null)
                    goto final;

                var photoUri = GlobalSettings.MEDIA_PATH.Replace("[Name]", name);

                return new OkObjectResult(photoUri);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            final:
            return BadRequest("Null");
        }
    }
}
