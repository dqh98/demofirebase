﻿using Microsoft.AspNetCore.Mvc;
using AppFirebase.Models;
using AppFirebase.Configurations;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json;

namespace AppFirebase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IFirebaseClient client = Utils.Config.ConfigClient();

        [HttpPost("add")]
        public async Task<IActionResult> Add(Users input)
        {
            var dataInput = new Dictionary<string, Users>();
            dataInput.Add(Guid.NewGuid().ToString(), input);
            try
            {
                //Thêm dữ liệu
                await client.UpdateAsync("users", dataInput);

                //Lấy ra dữ liệu
                var response = await client.GetAsync("users");
                var result = JsonConvert.DeserializeObject<Dictionary<string, Users>>(response.Body);
                return new OkObjectResult(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
