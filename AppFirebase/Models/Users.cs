﻿namespace AppFirebase.Models
{
    public class Users
    {
        public string? name { get; set; }
        public string? email { get; set; }
        public int? age { get; set; }
    }
}
