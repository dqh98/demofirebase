﻿using AppFirebase.Configurations;
using FireSharp.Config;
using FireSharp.Interfaces;

namespace AppFirebase.Utils
{
    public static class Config
    {
        public static IFirebaseClient ConfigClient()
        {
            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = GlobalSettings.AUTH_SECRET,
                BasePath = GlobalSettings.BASE_PATH,
            };
            IFirebaseClient client = new FireSharp.FirebaseClient(config);
            return client;
        }
    }
}
